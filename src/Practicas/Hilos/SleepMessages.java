/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas.Hilos;

/**
 *
 * @author Vicente Lara Mtz
 */
public class SleepMessages {
    public static void main(String args[])
        throws InterruptedException {
        String importantInfo[] = {
            "Las yeguas comen avena",
            "Dos comen avena",
            "Pequeños chivos comen hierba",
            "Un chico comera hierba tambien"
        };

        for (int i = 0; i < importantInfo.length;  i++) {
            //Pause for 4 seconds
            Thread.sleep(4000);
            //Print a message
            System.out.println(importantInfo[i]);
        }
    }
}
