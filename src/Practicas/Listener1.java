/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author chente
 */
public class Listener1 extends JFrame {
    JButton btn1; // View
    
    public Listener1(){
        this.setLayout(new BorderLayout());
        btn1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        btn1.addActionListener(new MiListener());
        
        this.add(btn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener1 frm = new Listener1();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
}

// Clase controller
class MiListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
    }
}
