/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author chente
 */
public class Listener2 extends JFrame implements ActionListener {
    JButton btn1; // View
    
    public Listener2(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        btn1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        btn1.addActionListener(this);
        
        this.add(btn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener2 frm = new Listener2();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
    }
}
