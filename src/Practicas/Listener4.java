/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author gmendez
 */
public class Listener4 extends JFrame {
    JButton btn1; // View
    
    public Listener4(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        btn1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        btn1.addMouseListener( new MouseAdapter(){                                    
         public void mouseClicked(MouseEvent arg0) {
        System.exit(0);
          }
          });
        
        this.add(btn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener4 frm = new Listener4();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
}