/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author chente
 */
public class Listener3 extends JFrame {
    JButton btn1; // View
    
    public Listener3(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        btn1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        btn1.addActionListener((ActionEvent arg0) -> {
          System.exit(0);
          });
        
        this.add(btn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener3 frm = new Listener3();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
}