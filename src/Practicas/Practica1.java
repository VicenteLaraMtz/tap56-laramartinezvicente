
package Practicas;
import java.awt.BorderLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


/**
 *
 * @author Vicente Lara Mtz
 */
public class Practica1 extends JFrame {
    JLabel label;
    JTextField text;
    JButton boton; 
    
    public Practica1(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        this.setLayout(new BorderLayout());
                        
        
        text = new JTextField();
        boton = new JButton("Saludar");
        
        // Aqui le pegamos el controlador al boton
        boton.addActionListener((ActionEvent arg0) -> {
          JOptionPane.showMessageDialog(null, "Hola "+text.getText()+" Que tengas un buen dia");
                                });
        
        this.add(boton, BorderLayout.PAGE_START);
        this.add(text,  BorderLayout.CENTER);
        this.add(boton, BorderLayout.PAGE_END);        
    }
    
    public static void main(String args[]){
        Practica1 frm = new Practica1();
        frm.setSize(500,300);
        frm.setVisible(true);        
    }
}
